# Testo compiti proposti durante il corso

## L0 (1 es.)

- Indice dei libri proibiti vs Fake news (3000 car. circa)



## L1 (1 es.)

- Analisi di servizi digitali (3000 car. circa)



## L2 (2 es.)

- Trovare info aggiornate sul digital divide in Italia, Europa, Mondo
	basta un elenco "ragionato" (i.e., commentato) di alcune fonti interessanti, ad es. enti che fanno monitoraggio, "indici" pubblicati annualmente, etc.

- Proporre propria lista di servizi di accesso/cittadinanza digitale (3000 car. circa)
	vale sia in positivo (cosa vorrei che fosse un servizio di base di cdt) sia in negativo (cosa NON mi pare abbia rango di servizio di base), argomentando



## L3 (2 es.)

- studiare PNSD e mappare argomenti/idee/proposte/etc. con Arcobaleno

- leggere Code Is Law (Lessig) e scrivere circa 1000 (mi sono reso conto che 500 era veramente poco, cmq se avete già scritto lasciate pure così) caratteri di commento, anche googlando articoli contrari

- (precompito) scartabellare siti opendata (dati.gov.it, dati.comune.milano.it, dati.lombardia.it, data.gov.uk e altri) facendosi incuriosire su tematiche su cui poi lavorare (farsi una domanda e darsi una risposta basata sui dati)


## L4 (2 es.)

- curiosare nei siti opendata e farsi venire in mente un quesito la cui risposta è "calcolabile" interrogando/incrociando qualche dataset, impostare un micro-progetto che descriva come potrebbe essere realizzato il software che calcola la risposta (max 3000 caratteri)


# NOTA IMPORTANTE PER I LIVELLI ALTI

Questo venerdì 22/11 ci sarà un incontro tecnico su Decidim (presenti gli sviluppatori) durante la giornata "i nuovi codici della democrazia: software, partecipazione civica e democrazia nell’era digitale" (https://www.milanopartecipa.com/i-nuovi-codici-della-democrazia-software-partecipazione-civica-e-democrazia-nellera-digitale/), vi preghiamo di partecipare (verrà considerato parte integrante del corso) per capire l'architettura della piattaforma stessa.
