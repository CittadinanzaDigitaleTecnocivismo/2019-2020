# Note in libertà, procedurali, ecc

Questo README ci serve da lavagna condivisa per le note di lavoro, man mano metto qualche info su come usare git (es. https://git-scm.com/book/en/v2) e su come produrre gli articoletti usando LaTeX (es. https://www.latex-project.org/about/)

Nota bene: NON abbiate paura di scrivermi mail/telegram/etc. specie se avete dubbi sui miei commenti nel repo o se volete comunicare le vostre intenzioni nei confronti dell'esame ("quando mi presento", "che approfondimento faccio", etc.). Va bene anche un post sul sito http://tecnocivismo.di.unimi.it (nel tab "laboratorio" ad esempio) così anche altri possono beneficiare delle risposte.
Ho aggiunto un'esortazione nel post: http://tecnocivismo.di.unimi.it/infodiscs/view/2064

**PER LE DATE EFFETTIVE IN CUI PRESENTARSI: dato che le concordiamo e che alcuni di voi vorrebbero venire a sentire chi viene ~~torturato~~ascoltato in una data sessione usiamo il sito (in particolare http://tecnocivismo.di.unimi.it/infodiscs/view/2057) del corso per coordinare le date**

* PRIMA DELL'ESAME: verificate voi di aver esaurito tutti i compiti specificati, io farò controllo solo all'atto della presentazione e se manca qualcosa non vi accetto

## Elaborati

* IMPORTANTISSIMO: NON COPINCOLLATE testo dalla rete senza citarlo (i.e., ambiente di quoting + riferimento bibliografico, cfr. il mio template)! Se trovo ancora roba copincollata senza citazione corretta annullo il compito
* IMPORTANTE: cercate di essere SINTETICI ma ESAUSTIVI
* io man mano cerco di analizzare ciò che scrivete, non è detto che riesca a farlo completamente di volta in volta, ma entro la fine del corso avrò letto e commentato tutti, casomai ci fosse qualche dubbio urgente segnalatemelo a lezione (il motivo per cui vi ho detto di lasciare i todo commentati è proprio per capire se devo evadere cose o meno)
* Cercate di non "sbrodolare testo", non c'è bisogno di scrivere tanto, ma di scrivere bene!
* il senso di fare un repo unico è quello di stimolarvi a consultare il lavoro fatto dai vostri colleghi, soprattutto nei commenti in itinere! In questo senso guardate i commit messages e fate un check dei todo che metto sui vostri colleghi ('grep' is your friend!)
* IMPORTANTE: il senso di darvi questi compiti è farvi riflettere su alcuni temi citati a lezione, "costringendovi" a cercare documentazione in rete, in modo da "fare vostro" un argomento. Ogni compito va "evaso" avendo in mente lo scopo di "capire il senso" più che quello di "far vedere al docente che gli scrivo le cose che vuole".

## GIT

* Mi raccomando fate sempre un PULL prima di un PUSH!
* cercate di scrivere messaggi di commit che abbiano un minimo di senso (non "quinto commit")
* NON pushate i PDF, ma i LaTeX!
* NON cancellate i commenti miei o degli altri, semplicemente commentateli, così rimane traccia delle cose dette e risposte (senza dover andare indietro nel versioning)

## LaTeX

* cercate di leggervi qualche intro su LaTeX in modo da non scrivere sintassi troppo strampalata...
* Cercate di scrivere i paragrafi su più righe (anche brevi), tanto LaTeX li tiene comunque uniti, avremo così meno collisioni su git
* NON usate spazi o caratteri strani nei nomi dei file
* ATTENZIONE alle "duplicate entries" nel file bib!
* I nomi dei file devono iniziare con "L<livello>_" in modo da avere il colpo d'occhio
* NON c'è bisogno di usare \\ in fondo ai paragrafi, basta mettere una riga vuota, il \\ si usa per un a capo continuato
* Le accentate sono supportate da LaTeX, potete usarle senza dover scrivere ad esempio \`a
* le citazioni vanno prima del punto di fine paragrafo, es. "testo testo testo \cite{key}."

## FORMATO DEI COMMENTI/TAGS:

 \todo{<nickname di chi scrive>: testo del commento}

IMPORTANTE: marcate (subito *sotto* al titolo della sezione) le sezioni pronte da leggere con un:
 \todo{TOREAD FdC} se per Fiorella De Cindio
 \todo{TOREAD} se per atrent
 
Quando le ho lette e valutate le marco con:
 \todo{TOEDIT}

Quando le ho lette e valutate e le ritengo 'final' le marco con [ATOK] nel titolo, più qualche commento per me da utilizzare all'orale

NOTA BENE: un [ATOK] non vuol dire che il giudizio è automaticamente "voto massimo", vuol dire solo che il taglio del pezzo è corretto, che ci sono i riferimenti, che sono state evase le mie osservazioni, etc.

IMPORTANTE: alla fine del lavoro ogni sezione dovrà essere passata da una mia lettura e approvazione, cioè ogni titolo dovrà contenere il mio tag, ergo cercate di mettere i TOREAD


